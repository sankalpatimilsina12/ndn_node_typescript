# NDN Node (Typescript)

Named data networking flow demonstration in Node JS.

### Prerequisites
The following software must be installed in the system for this
project to work.

```
1. Named Data Networking Forwading Daemon (NFD)
2. Ndn-ccx (NDN C++ library)
3. Node (12.x.x). Seems to have problem with the latest major
release. For safety, use 12.0.0 (tested).
```

### Installing

```
1. npm install
```

### Description
The project tries to demonstrate data fetch and publish
between a producer and consumer in NDN. We are assuming to be
both the producer and consumer.

## Running 

Run the producer
```
1. npm run producer
```

Run the consumer
```
2. npm run consumer
```


