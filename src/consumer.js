import {Forwarder} from "@ndn/fw";
import {UnixTransport} from "@ndn/node-transport";
import {L3Face} from "@ndn/l3face";
import {Endpoint} from "@ndn/endpoint";
import {Interest, Name} from "@ndn/packet";

async function expressInterest(fw, i) {
    // Start the consumer
    try {
        const data = await new Endpoint({fw: fw}).consume(
            new Interest("/hello/test", Interest.MustBeFresh)
        );
        const payloadText = new TextDecoder().decode(data.content);
        console.log(`Received ${i}`, `${data.name} Content: ${payloadText}`);
    } catch (e) {
        console.error("Interest expired or timeout");
    }
}

async function* asyncGenerator() {
    let i = 0;
    while (i < 50) {
        yield i++;
    }
}

async function main() {
    const fw = Forwarder.create();

    let transport;
    try {
        transport = await UnixTransport.connect("/var/run/nfd.sock");
    } catch (err) {
        console.warn("NFD not running");
        return;
    }

    const face = fw.addFace(new L3Face(transport));
    face.addRoute(new Name("/"));

    // Express interest 50 times/sec
    setInterval(() => {
        (async function () {
            for await (let i of asyncGenerator()) {
                await expressInterest(fw, i);
            }
        })();
    }, 1000);
}


main();
