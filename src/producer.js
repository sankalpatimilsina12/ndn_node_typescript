import {enableNfdPrefixReg} from "@ndn/nfdmgmt";
import {Forwarder} from "@ndn/fw";
import {UnixTransport} from "@ndn/node-transport";
import {L3Face} from "@ndn/l3face";
import {EcPrivateKey} from "@ndn/keychain";
import {Endpoint} from "@ndn/endpoint";
import {Data} from "@ndn/packet";

async function main() {
    const fw = Forwarder.create();

    let transport;
    try {
        transport = await UnixTransport.connect("/var/run/nfd.sock");
    } catch (err) {
        console.warn("NFD not running");
        return;
    }

    const face = fw.addFace(new L3Face(transport));

    const [privateKey] = await EcPrivateKey.generate("/K", "P-256");
    enableNfdPrefixReg(face, {signer: privateKey});

    // Start the producer. Wait for 40 ms (ie. answering only 1000/40 = 25 requests/sec).
    new Endpoint({fw}).produce("/hello/test", async () => {
        await new Promise(r => setTimeout(r, 40));
        return new Data("/hello/test", Data.FreshnessPeriod(1), new TextEncoder().encode("Data"));
    });
}

main();
