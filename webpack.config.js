const path = require("path");
const util = require("util");

module.exports = {
    mode: 'development',
    entry: {
        'consumer': ['@babel/polyfill', './src/consumer.js'],
        'producer': ['@babel/polyfill', './src/producer.js']
    },
    target: "node",
    output: {
        path: path.resolve(__dirname, "dist/"),
        filename: '[name].js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['@babel/preset-env'],
                }
            }
        ]
    }
};